(* OCAML VERSION : 4.05.0 
LAMBDA CALCUL SIMPLEMENT TYPE
Léo OLIVIER & Kylian LOUSSOUARN *)

(* Types primitifs : entiers, 
booléens et fonction *)
type lamType =
  | Boolean
  | Integer
  | Fun of lamType * lamType
;;

(* Types d'une lambda expression :
application, abstraction (lambda), 
variable, addition, IF logique, 
types primitifs *)
type lamExpr =
  | App of lamExpr * lamExpr
  | Lambda of string * lamType * lamExpr
  | Var of string
  | Plus of lamExpr * lamExpr
  | Minus of lamExpr * lamExpr
  | Mult of lamExpr * lamExpr
  | If of lamExpr * lamExpr * lamExpr
  | Int of int
  | Bool of bool
;;

(* Vérifie qu'une lambda expression est bien typée *)
let typecheck expr =
  let rec aux list expr =
    match expr with 
    | App(expr1, expr2) -> (* Application *)
        let term = aux list expr1 in
        begin match term with 
          | Fun(subterm1, subterm2) -> (* Application de fonctions *)
              if subterm1 <> subterm2 then
                failwith "Non-concordance des types sur une application de fonctions"
              else
                subterm2
          | _ -> failwith "Application sur autre chose qu'une fonction" 
        end
    | Lambda(x, t, e) -> (* Fonction : on ajoute à la liste la variable de la fonction *)
        Fun(t, aux ((x,t)::list) e)
    | Var x  -> (* Variable : on cherche le terme correspondante dans la liste *)
        let (y, term) = List.find (fun (y, term) -> y = x) list in term
    | Plus(expr1, expr2) -> (* Addition *)
        let term1 = aux list expr1 in
        let term2 = aux list expr2 in
        begin match (term1, term2) with 
          | (Integer, Integer) -> Integer (* L'addition n'est possible que sur des entiers *)
          | _ -> failwith "Addition sur autre chose que des entiers"
        end
    | Minus(expr1, expr2) -> (* Soustraction *)
        let term1 = aux list expr1 in
        let term2 = aux list expr2 in
        begin match (term1, term2) with
          | (Integer, Integer) -> Integer (* La soustraction n'est possible que sur des entiers *)
          | _ -> failwith "Soustraction sur autre chose que des entiers"
        end
    | Mult(expr1, expr2) -> (* Multiplication *)
        let term1 = aux list expr1 in
        let term2 = aux list expr2 in
        begin match (term1, term2) with
          | (Integer, Integer) -> Integer (* La multiplication n'est possible que sur des entiers *)
          | _ -> failwith "Multiplication sur autre chose que des entiers"
        end
    | If(expr1, expr2, expr3) -> (* Conditionnelle *)
        let term1 = aux list expr1 in
        let term2 = aux list expr2 in
        let term3 = aux list expr3 in
        begin match (term1, term2, term3) with 
          | (Boolean, Boolean, Boolean) -> Boolean (* Si bool alors bool sinon bool *) 
          | (Boolean, Integer, Integer) -> Integer (* Si bool alors int sinon int *)
          | _ -> failwith "Conditionnelle avec des types de retour différents ou une condition non booléenne"
        end
    | Int _ -> Integer
    | Bool _ -> Boolean
  in aux [] expr
;;

(* Type de retour d'une lambda expression *)
type lamValue = 
  | VBool of bool
  | VInt of int
  | VFun of ((string * lamValue) list -> lamValue -> lamValue)
;;

(* Évaluation d'une lambda expression *)
let rec eval expr = 
  let rec aux list expr =
    match expr with
    | App(expr1, expr2) -> (* Application *)
        let term1 = aux list expr1 in
        let term2 = aux list expr2 in
        begin match term1 with 
          | VFun a1 -> a1 list term2
          | _ -> failwith "Application sur autre chose qu'une fonction" 
        end
    | Lambda(x, t, e) -> (* Fonction : on ajoute la variable de la fonction à la liste *)
        VFun (fun list' v' -> aux ((x, v')::list') e) 
    | Var x  -> (* Variable : on cherche le terme correspondante dans une fonction *)
        let (y, value) = List.find (fun (y, term) -> y = x) list in value
    | Plus(expr1, expr2) -> (* Addition *)
        let term1 = aux list expr1 in
        let term2 = aux list expr2 in
        begin match (term1, term2) with 
          | (VInt int1, VInt int2) -> VInt (int1 + int2) 
          | _ -> failwith "Addition sur autre chose que des entiers"
        end
    | Minus(expr1, expr2) -> (* Soustraction *)
        let term1 = aux list expr1 in
        let term2 = aux list expr2 in
        begin match (term1, term2) with 
          | (VInt int1, VInt int2) -> VInt (int1 - int2) 
          | _ -> failwith "Soustraction sur autre chose que des entiers"
        end
    | Mult(expr1, expr2) -> (* Multiplication *)
        let term1 = aux list expr1 in
        let term2 = aux list expr2 in
        begin match (term1, term2) with 
          | (VInt int1, VInt int2) -> VInt (int1 * int2) 
          | _ -> failwith "Multiplication sur autre chose que des entiers"
        end
    | If(expr1, expr2, expr3) -> (* Conditionnelle *)
        let term1 = aux list expr1 in
        let term2 = aux list expr2 in 
        let term3 = aux list expr3 in
        begin match (term1, term2, term3) with 
          | (VBool bool1, VBool bool2, VBool bool3) -> VBool (if bool1 then bool2 else bool3) 
          | (VBool bool1, VInt int2, VInt int3) -> VInt (if bool1 then int2 else int3)
          | _ -> failwith "Conditionnelle avec des types de retour différents ou une condition non-booléenne"
        end
    | Int int -> VInt int
    | Bool bool -> VBool bool
  in aux [] expr
;;

(* Printer *)
let rec eval_to_str e =
  match e with 
  | VInt i-> "Integer" ^ "(" ^ string_of_int i ^ ")" 
  | VBool b-> "Boolean" ^ "(" ^ string_of_bool b ^ ")"  
  |_ -> "ni Int ni Boolean";;

(* Fonction x + x *)
let plus x = App(Lambda("x", Integer, Plus(Var "x", Var "x")), x);;
let typecheck_x = typecheck (Lambda("x", Integer, Plus(Var "x", Var "x")));;
let eval_plus = eval (plus (Int 4));; (* 8 *)
print_string (eval_to_str (eval_plus));;

(* Fonction x + y *)
let fun_plus x y = App(Lambda("x", Integer, App(Lambda("y", Integer, Plus(Var "x", Var "y")), y)), x);; 
let eval_plus = eval (fun_plus (Int 4) (Int 5));; (* 9 *)
print_string (eval_to_str (eval_plus));;

(* Fonction x - y *)
let fun_minus x y = App(Lambda("x", Integer, App(Lambda("y", Integer, Minus(Var "x", Var "y")), y)), x);; 
let eval_minus = eval (fun_minus (Int 4) (Int 5));; (* -1 *)
print_string (eval_to_str (eval_minus));;

(* Fonction qui renvoie vrai *)
let fun_true x y = App(Lambda("x", Boolean, App(Lambda("y", Boolean, Var "x"), y)), x);; 
let typecheck_true = typecheck (App(Lambda("x", Boolean, App(Lambda("y", Boolean, Var "x"), Var "y")), Var "x"));;
let eval_true = eval (fun_true (Bool true) (Bool false));; (* true *)
print_string (eval_to_str (eval_true));;

(* Fonction qui renvoie faux *)
let fun_true x y = App(Lambda("x", Boolean, App(Lambda("y", Boolean, Var "y"), y)), x);; 
let eval_true = eval (fun_true (Bool true) (Bool false));; (* false *)
print_string (eval_to_str (eval_true));;

(* NOT *)
let not p = App(Lambda("p", Boolean, App(Lambda("a", Boolean, App(Lambda("b", Boolean, If(Var "p", Var "a", Var "b")), (Bool true))), (Bool false))), p);;
let eval_not = eval (not (Bool true));; (* false *)
print_string (eval_to_str (eval_not));;

(* AND *)
let and_operator p q = App(Lambda("p", Boolean, App(Lambda("q", Boolean, If(Var "p", Var "q", Var "p")), q)), p);;
let eval_and = eval (and_operator (Bool true) (Bool true));; (* true *)
print_string (eval_to_str (eval_and));;

(* OR *)
let or_operator p q = App(Lambda("p", Boolean, App(Lambda("q", Boolean, If(Var "p", Var "p", Var "q")), q)), p);;
let eval_or = eval (or_operator (Bool false) (Bool false));; (* false *) 
print_string (eval_to_str (eval_or));;
